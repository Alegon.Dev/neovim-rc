1 - install packer with:
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim

2 - remove/changeName of "after" folder

3 - from packer.lua run :so and :PackerSync after

4 - put back "after" folder

5 - :Mason and install LSPs

5 - :so inside every file

6 - to make system clipboard work on arch linux install "xclip" for x11 or "wl-clipboard" for wayland
    (echo $XDG_SESSION_TYPE to know which one is running)
