local telescope = require("telescope")
local flutterTools = require("flutter-tools")
flutterTools.setup {
  widget_guides = {
    enabled = true,
  },
}
telescope.load_extension("flutter")
vim.keymap.set("n", "<F5>", telescope.extensions.flutter.commands)
