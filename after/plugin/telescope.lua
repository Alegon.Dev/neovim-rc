local builtin = require('telescope.builtin')

function  GrepSearch()
	builtin.grep_string({search = vim.fn.input("Grep > ")});
end

vim.keymap.set('n', '<C-p>', builtin.git_files, {})
vim.keymap.set('n', '<C-f>', GrepSearch)
vim.keymap.set('n', '<C-i>', builtin.help_tags, {})
