local lsp = require("lsp-zero")

lsp.preset("recommended")

lsp.ensure_installed({
  'tsserver',
  'eslint',
  'lua_ls',
  'rust_analyzer',
})

-- Fix Undefined global 'vim'
lsp.configure('lua_ls', {
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            }
        }
    }
})


local cmp = require('cmp')
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ['<C-k>'] = cmp.mapping.select_prev_item(cmp_select),
  ['<C-j>'] = cmp.mapping.select_next_item(cmp_select),
  ['<C-l>'] = cmp.mapping.confirm({ select = true }),
  ["<C-Space>"] = cmp.mapping.complete(),
})

-- disable completion with tab
-- this helps with copilot setup
-- cmp_mappings['<Tab>'] = nil
-- cmp_mappings['<S-Tab>'] = nil
cmp_mappings['<CR>'] = nil

lsp.setup_nvim_cmp({
  mapping = cmp_mappings
})

lsp.set_preferences({
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})

-- local opts = {remap = false}
vim.keymap.set("n", "'", vim.lsp.buf.definition)
vim.keymap.set("n", ".", vim.lsp.buf.code_action)
vim.keymap.set("n", ";", vim.lsp.buf.hover)
vim.keymap.set("n", ",", vim.diagnostic.open_float)
vim.keymap.set("n", "[d", vim.diagnostic.goto_next)
vim.keymap.set("n", "]d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "<leader>dl", vim.diagnostic.setqflist)

-- Variable references
vim.keymap.set("n", "<leader>vrr", vim.lsp.buf.references, opts)
-- Variable rename
vim.keymap.set("n", "<leader>vrn", vim.lsp.buf.rename, opts)

-- vim.keymap.set("n", "<leader>vws", vim.lsp.buf.workspace_symbol, opts)
-- vim.keymap.set("i", "<C-h>", vim.lsp.buf.signature_help, opts)

-- lsp.on_attach(function(client, bufnr)
--
--   if client.name == "eslint" then
--       vim.cmd.LspStop('eslint')
--       return
--   end
--
-- end)

lsp.setup()

vim.diagnostic.config({
    virtual_text = true,
})
